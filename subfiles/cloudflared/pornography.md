## How many % of pornographic domains are using Cloudflare?


We downloaded the pornhosts list from [here](https://mypdns.org/my-privacy-dns/porn-records) and filter out duplicates.
Here's the result.


[//]: # (start replacement)


- Top 5 TLDs

| TLD | Count |
| --- | --- |
| com | 9,123 |
| net | 1,122 |
| org | 308 |
| pro | 243 |
| tv | 213 |


- Cloudflare %

| Type | Count |
| --- | --- |
| Cloudflare | 5,617 |
| Normal | 8,379 |


### 40.1% of pornographic domains are using Cloudflare.
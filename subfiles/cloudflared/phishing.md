## How many % of phishing domains are using Cloudflare?


We downloaded the Phishing Domains from [here](https://github.com/mitchellkrogza/Phishing.Database) and filter out duplicates.
Here's the result.


[//]: # (start replacement)


- Top 5 TLDs

| TLD | Count |
| --- | --- |
| com | 29,847 |
| net | 2,062 |
| org | 1,772 |
| top | 1,548 |
| br | 1,508 |


- Cloudflare %

| Type | Count |
| --- | --- |
| Cloudflare | 3,704 |
| Normal | 51,742 |


### 6.7% of phishing domains are using Cloudflare.